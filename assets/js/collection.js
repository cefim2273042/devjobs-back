function addDetailsRole() {
    const list = document.querySelector('.details-role-list');
    const count = list.querySelectorAll('div').length;
    const newWidget = list.dataset.prototype.replace(/__name__/g, count);

    const newDiv = document.createElement('div');
    newDiv.classList.add('form-group');
    newDiv.innerHTML = newWidget;

    list.appendChild(newDiv);
}

function addDetailsPrerequis() {
    const list = document.querySelector('.details-prerequis-list');
    const count = list.querySelectorAll('div').length;
    const newWidget = list.dataset.prototype.replace(/__name__/g, count);

    const newDiv = document.createElement('div');
    newDiv.classList.add('form-group');
    newDiv.innerHTML = newWidget;

    list.appendChild(newDiv);
}

function removeDetailsRole(btn) {
    const div = btn.closest('.form-group');
    div.remove();
}

const addRoles = document.querySelector('.add-details-role-btn');
const addPrerequis = document.querySelector('.add-details-prerequis-btn');

addRoles.addEventListener('click', () => {
    addDetailsRole();
});

addPrerequis.addEventListener('click', () => {
    addDetailsPrerequis();
});

const removeButtons = document.querySelectorAll('.remove-details-role-btn');
removeButtons.forEach((btn) => {
    btn.addEventListener('click', () => {
        removeDetailsRole(btn);
    });
});