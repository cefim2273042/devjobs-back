<?php

namespace App\Form;

use App\Entity\Offer;
use App\EventSubscriber\OfferFormSubscriber;
use App\Form\Default\PrerequisType;
use App\Form\Default\RoleType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OfferType extends AbstractType
{
    private OfferFormSubscriber $offerFormSubscriber;

    public function __construct(OfferFormSubscriber $offerFormSubscriber)
    {
        $this->offerFormSubscriber = $offerFormSubscriber;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('postName', TextType::class, [
                'label' => 'Post name',
                'attr' => [
                    'placeholder' => 'Ex: photographer',
                    'class' => 'form-control',
                ],
            ])
            ->add('contractType', ChoiceType::class, [
                'required' => true,
                'attr' => ['class' => 'form-select', 'placeholder' => 'Contract type'],
                'choices' => [
                    'Full time' => 1,
                    'Part Time' => 2,
                    'Freelance' => 3,
                    'Stage' => 4,
                ],
            ])
            ->add('applyUrl', UrlType::class, [
                'attr' => ['class' => 'form-control', 'placeholder' => 'Ex: blah.fr'],
                'label' => 'Apply url',
                'disabled' => true,
            ])
            ->add('descr', TextareaType::class, [
                'required' => true,
                'attr' => ['class' => 'form-control', 'placeholder' => 'Ex: Lorem ipsum'],
                'label' => 'Job description',
            ])
            ->add('prerequis', TextareaType::class, [
                'required' => true,
                'attr' => ['class' => 'form-control', 'placeholder' => 'Ex: Lorem ipsum'],
            ])
            ->add('prerequisArray', CollectionType::class, [
                'entry_type' => PrerequisType::class,
                'entry_options' => ['label' => false],
                'label' => false,
                'attr' => ['class' => 'form-control details-prerequis-list', 'placeholder' => 'Ex: Lorem ipsum'],
                'allow_add' => true,
            ])
            ->add('role', TextareaType::class, [
                'required' => true,
                'attr' => ['class' => 'form-control', 'placeholder' => 'Ex: Lorem ipsum'],
                'label' => 'Role description',
            ])
            ->add('roleArray', CollectionType::class, [
                'entry_type' => RoleType::class,
                'entry_options' => ['label' => false],
                'label' => false,
                'attr' => ['class' => 'form-control details-role-list', 'placeholder' => 'Ex: Lorem ipsum'],
                'allow_add' => true,
            ])
            ->add('isActive', ChoiceType::class, [
                'label' => 'Status',
                'attr' => ['class' => 'form-select'],
                'choices' => [
                    'Published' => 1,
                    'Draft' => 2,
                    'Waiting for review' => 3,
                ],
            ])
            ->addEventSubscriber($this->offerFormSubscriber)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Offer::class,
        ]);
    }
}
