<?php

namespace App\Form\RegisterType;

use App\Entity\CompanyUser;
use Karser\Recaptcha3Bundle\Form\Recaptcha3Type;
use Karser\Recaptcha3Bundle\Validator\Constraints\Recaptcha3;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Regex;

class CompanyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'label' => 'Company name',
                'attr' => ['class' => 'form-control', 'placeholder' => 'Ex: IRHT'],
            ])
            ->add('logo', FileType::class, [
                'label' => 'Company logo',
                'required' => false,
                'mapped' => false,
                'attr' => ['class' => 'form-control', 'placeholder' => 'Ex: Faudra revoir ce champs'],
                'constraints' => [
                    new File([
                        'maxSize' => '2024k',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                            'image/gif',
                            'image/webp',
                            'image/svg+xml',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid image file (JPEG, PNG, GIF, WEBP).',
                    ]),
                ],
            ])
            ->add('color', ColorType::class, [
                'required' => true,
                'label' => 'Color background for your logo',
                'attr' => ['class' => 'form-control form-control-color'],
            ])
            ->add('location', CountryType::class, [
                'required' => true,
                'label' => 'Location of your Company',
                'attr' => ['class' => 'form-select'],
            ])
            ->add('website', TextType::class, [
                'required' => true,
                'label' => 'Website of your Company',
                'attr' => ['class' => 'form-control', 'placeholder' => 'Ex: https://www.irht.fr'],
            ])
            ->add('lastname', TextType::class, [
                'required' => true,
                'label' => 'Lastname contact',
                'attr' => ['class' => 'form-control', 'placeholder' => 'Ex: Dupont'],
            ])
            ->add('firstname', TextType::class, [
                'required' => true,
                'label' => 'Firstname contact',
                'attr' => ['class' => 'form-control', 'placeholder' => 'Ex: Jean'],
            ])
            ->add('email', EmailType::class, [
                'required' => true,
                'label' => 'Email contact',
                'attr' => ['class' => 'form-control', 'placeholder' => 'Ex: name.name@domain.fr'],
                'constraints' => [
                    new Regex("/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z\-0-9]{2,}))$/", 'You did not enter a valid email'),
                ],
            ])
            ->add('username', TextType::class, [
                'required' => true,
                'label' => 'Username for login',
                'attr' => ['class' => 'form-control', 'placeholder' => 'Ex: jdupont'],
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'required' => true,
                'first_options' => ['label' => 'Password', 'hash_property_path' => 'password'],
                'second_options' => ['label' => 'Repeat Password'],
                'invalid_message' => 'Password fields must match',
                'mapped' => false,
                'constraints' => [
                    new Regex('/^(?=(?:.*[A-Z]))(?=(?:.*[a-z]))(?=(?:.*\d))(?=(?:.*[!@#$%^&*()\-_=+{};:,<.>]))([A-Za-z0-9!@#$%^&*()\-_=+{};:,<.>]{6,20})$/', 'Password must contain at least 1 uppercase and 1 lowercase letter, 1 special character, 1 number, and must be between 6 and 20 characters'),
                ],
                'options' => ['attr' => ['class' => 'form-control']],
            ])
            ->add('captcha', Recaptcha3Type::class, [
                'constraints' => new Recaptcha3(),
                'action_name' => 'register',
                'locale' => 'en',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CompanyUser::class,
        ]);
    }
}
