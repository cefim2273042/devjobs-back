<?php

namespace App\Form\Applicant;

use App\Entity\Applicants;
use App\EventSubscriber\ApplicantFormSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Regex;

class ApplicantType extends AbstractType
{
    private ApplicantFormSubscriber $applicantFormSubscriber;

    public function __construct(ApplicantFormSubscriber $applicantFormSubscriber)
    {
        $this->applicantFormSubscriber = $applicantFormSubscriber;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('lastname', TextType::class, [
                'required' => true,
                'label' => 'Lastname of the applicant',
                'attr' => ['class' => 'form-control', 'placeholder' => 'Ex: Dupont'],
            ])
            ->add('firstname', TextType::class, [
                'required' => true,
                'label' => 'Firstname of the applicant',
                'attr' => ['class' => 'form-control', 'placeholder' => 'Ex: Jean'],
            ])
            ->add('email', EmailType::class, [
                'required' => true,
                'label' => 'Email of the applicant',
                'attr' => ['class' => 'form-control', 'placeholder' => 'Ex: name.name@domain.fr'],
                'constraints' => [
                    new Regex("/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z\-0-9]{2,}))$/", 'You did not enter a valid email'),
                ],
            ])
            ->add('phoneNumber', TelType::class, [
                'required' => true,
                'label' => 'Phone number of the applicant',
                'attr' => ['class' => 'form-control', 'placeholder' => 'Ex: 0606060606'],
                'constraints' => [
                    new Regex("/^((\+)33|0)[1-9](\d{2}){4}$/", 'You did not enter a valid phone number'),
                ],
            ])
            ->add('resum', FileType::class, [
                'label' => 'Your resum',
                'required' => false,
                'mapped' => false,
                'attr' => ['class' => 'form-control'],
                'constraints' => [
                    new File([
                        'maxSize' => '2024k',
                        'mimeTypes' => [
                            'application/pdf',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid document file (PDF).',
                    ]),
                ],
            ])
            ->add('comment', TextareaType::class, [
                'required' => false,
                'label' => 'Comment',
                'attr' => ['class' => 'form-control', 'placeholder' => 'Ex: I want this job because...'],
            ])
            ->addEventSubscriber($this->applicantFormSubscriber)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Applicants::class,
        ]);
    }
}
