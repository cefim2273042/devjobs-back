<?php

namespace App\Form\Applicant;

use App\Entity\Applicants;
use App\Entity\Offer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Regex;

class ApplicantEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('lastname', TextType::class, [
                'required' => true,
                'label' => 'Lastname',
                'attr' => ['class' => 'form-control', 'placeholder' => 'Ex: Dupont'],
            ])
            ->add('firstname', TextType::class, [
                'required' => true,
                'label' => 'Firstname',
                'attr' => ['class' => 'form-control', 'placeholder' => 'Ex: Jean'],
            ])
            ->add('email', EmailType::class, [
                'required' => true,
                'label' => 'Email',
                'attr' => ['class' => 'form-control', 'placeholder' => 'Ex: name.name@domain.fr'],
                'constraints' => [
                    new Regex("/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z\-0-9]{2,}))$/", 'You did not enter a valid email'),
                ],
            ])
            ->add('comment', TextareaType::class, [
                'required' => false,
                'label' => 'Comment',
                'attr' => ['class' => 'form-control', 'placeholder' => 'Ex: I want this job because...'],
            ])
            ->add('offer', EntityType::class, [
                'class' => Offer::class,
                'choice_label' => 'postName',
                'label' => 'Offer to apply for',
                'attr' => ['class' => 'form-control'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Applicants::class,
        ]);
    }
}
