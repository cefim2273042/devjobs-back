<?php

namespace App\Form\Company;

use App\Entity\CompanyUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Regex;

class CompanyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('lastname', TextType::class, [
                'required' => true,
                'label' => 'Lastname contact',
                'attr' => ['class' => 'form-control', 'placeholder' => 'Ex: Dupont'],
            ])
            ->add('firstname', TextType::class, [
                'required' => true,
                'label' => 'Firstname contact',
                'attr' => ['class' => 'form-control', 'placeholder' => 'Ex: Jean'],
            ])
            ->add('email', EmailType::class, [
                'required' => true,
                'label' => 'Email contact',
                'attr' => ['class' => 'form-control', 'placeholder' => 'Ex: name.name@domain.fr'],
                'constraints' => [
                    new Regex("/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z\-0-9]{2,}))$/", 'You did not enter a valid email'),
                ],
            ])
            ->add('location', CountryType::class, [
                'required' => true,
                'label' => 'Location of your Company',
                'attr' => ['class' => 'form-select'],
            ])
            ->add('logo', FileType::class, [
                'label' => 'Company logo',
                'required' => false,
                'mapped' => false,
                'attr' => ['class' => 'form-control'],
                'constraints' => [
                    new File([
                        'maxSize' => '2024k',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/png',
                            'image/gif',
                            'image/webp',
                            'image/svg+xml',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid image file (JPEG, PNG, GIF, WEBP).',
                    ]),
                ],
            ])
            ->add('color', ColorType::class, [
            'required' => true,
            'label' => 'Color background for your logo',
            'attr' => ['class' => 'form-control form-control-color'],
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CompanyUser::class,
        ]);
    }
}
