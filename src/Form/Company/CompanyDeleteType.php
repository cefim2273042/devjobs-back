<?php

namespace App\Form\Company;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompanyDeleteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'disabled' => true,
                'label' => 'Company name',
                'attr' => ['class' => 'form-control', 'placeholder' => 'Ex: IRHT'],
            ])
            ->add('lastname', TextType::class, [
                'required' => true,
                'disabled' => true,
                'label' => 'Contact Last Name',
                'attr' => ['class' => 'form-control', 'placeholder' => 'Ex: Dupont'],
            ])
            ->add('firstname', TextType::class, [
                'required' => true,
                'disabled' => true,
                'label' => 'Contact First Name',
                'attr' => ['class' => 'form-control', 'placeholder' => 'Ex: Michel'],
            ])
            ->add('email', EmailType::class, [
                'required' => true,
                'label' => 'Email contact',
                'disabled' => true,
                'attr' => ['class' => 'form-control', 'placeholder' => 'Ex: name.name@domain.com'],
            ])
            ->add('motif', TextareaType::class, [
                'required' => false,
                'label' => 'Why do you want to delete your account ?',
                'attr' => ['class' => 'form-control', 'placeholder' => 'Ex: I want to delete my account because...'],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Delete my account',
                'attr' => ['class' => 'btn btn-danger mt-2'],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
