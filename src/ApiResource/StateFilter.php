<?php

namespace App\ApiResource;

use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use Doctrine\ORM\QueryBuilder;

final class StateFilter extends AbstractFilter
{
    protected function filterProperty(
        string $property,
        $value,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        Operation $operation = null,
        array $context = []): void
    {
        if ('fulltime' !== $property) {
            return;
        }

        $alias = $queryBuilder->getRootAliases()[0];
        $bool = filter_var($value, FILTER_VALIDATE_BOOLEAN);
        if (true === $bool) {
            $queryBuilder->andWhere(sprintf('%s.contractType = :fulltime', $alias))
                ->setParameter('fulltime', 1);
        }
    }

    public function getDescription(string $resourceClass): array
    {
        return [
            'fulltime' => [
                'property' => null,
                'type' => 'integer',
                'required' => false,
                'swagger' => [
                    'description' => 'Search for fulltime jobs',
                    'name' => 'Fulltime',
                    'type' => 'integer',
                ],
            ],
        ];
    }
}
