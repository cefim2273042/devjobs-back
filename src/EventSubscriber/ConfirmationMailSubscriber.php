<?php

namespace App\EventSubscriber;

use App\Service\JWTService;
use App\Service\SendMailService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;

class ConfirmationMailSubscriber implements EventSubscriberInterface
{
    private SendMailService $sendMailService;
    private JWTService $JWTService;

    public function __construct(SendMailService $sendMailService, JWTService $JWTService)
    {
        $this->sendMailService = $sendMailService;
        $this->JWTService = $JWTService;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'kernel.response' => 'onKernelResponse',
        ];
    }

    public function onKernelResponse(ResponseEvent $event): void
    {
        $request = $event->getRequest();
        $requestType = $request->attributes->get('_route');
        $response = $event->getResponse();

        if ('company_register' === $requestType && 303 === $response->getStatusCode()) {
            $user = $request->request->all('company');

            $header = [
                'typ' => 'JWT',
                'alg' => 'HS256',
            ];

            $payload = [
                'user_identifiant' => $user['username'],
            ];

            $token = $this->JWTService->generate($header, $payload, $_ENV['JWT_SECRET']);

            $this->sendMailService->sendMail(
                'no-reply@devjobs.com',
                $user['email'],
                'Activate your account',
                'register',
                compact('user', 'token')
            );
        }
    }
}
