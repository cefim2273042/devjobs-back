<?php

namespace App\EventSubscriber;

use App\Entity\Offer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class ApplicantFormSubscriber implements EventSubscriberInterface
{
    private TokenStorageInterface $tokenStorage;

    public static function getSubscribedEvents(): array
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
        ];
    }

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function preSetData(FormEvent $event): void
    {
        $form = $event->getForm();
        if ($this->tokenStorage->getToken()) {
            $user = $this->tokenStorage->getToken()->getUser();

            if ($user instanceof UserInterface && in_array('ROLE_ADMIN', $user->getRoles())) {
                $form->add('offer', EntityType::class, [
                    'attr' => ['class' => 'form-select'],
                    'choice_label' => 'postName',
                    'class' => Offer::class,
                ]);
            }
        }
    }
}
