<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use App\ApiResource\MultipleFieldsSearchFilter;
use App\ApiResource\StateFilter;
use App\Repository\OfferRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource]
#[GetCollection(
    uriTemplate: '/jobs/',
    paginationEnabled: true,
    paginationItemsPerPage: 12,
    paginationClientItemsPerPage: true,
    normalizationContext: ['groups' => ['offers:list']],
)]
#[Get(
    uriTemplate: '/job/{id}',
    normalizationContext: ['groups' => ['offers:detail']]
)]
#[ApiFilter(MultipleFieldsSearchFilter::class)]
#[ApiFilter(SearchFilter::class, properties: ['company.location'])]
#[ApiFilter(StateFilter::class)]
#[ORM\Entity(repositoryClass: OfferRepository::class)]
class Offer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 125)]
    #[Groups(['offers:list', 'offers:detail'])]
    private ?string $postName = null;

    #[ORM\Column]
    #[Groups(['offers:list', 'offers:detail'])]
    private ?\DateTimeImmutable $createdAt = null;

    #[ORM\Column]
    #[Groups(['offers:list', 'offers:detail'])]
    private ?int $contractType = null;

    #[ORM\Column(length: 175, nullable: true)]
    #[Groups(['offers:detail'])]
    private ?string $applyUrl = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    #[Groups(['offers:detail'])]
    private ?string $descr = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(['offers:detail'])]
    private ?string $prerequis = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['offers:detail'])]
    private array $prerequisArray = [];

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(['offers:detail'])]
    private ?string $role = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['offers:detail'])]
    private array $roleArray = [];

    #[ORM\Column]
    private ?bool $isActive = null;

    #[ORM\Column(length: 125)]
    private ?string $slug = null;

    #[ORM\ManyToOne(inversedBy: 'offers')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['offers:list', 'offers:detail'])]
    private ?CompanyUser $company = null;

    #[ORM\OneToMany(mappedBy: 'offer', targetEntity: Applicants::class, orphanRemoval: true)]
    private Collection $applicants;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable('now');
        $this->applicants = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPostName(): ?string
    {
        return $this->postName;
    }

    public function setPostName(string $postName): static
    {
        $this->postName = $postName;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getContractType(): ?int
    {
        return $this->contractType;
    }

    public function setContractType(int $contractType): static
    {
        $this->contractType = $contractType;

        return $this;
    }

    public function getApplyUrl(): ?string
    {
        return $this->applyUrl;
    }

    public function setApplyUrl(string $applyUrl): static
    {
        $this->applyUrl = $applyUrl;

        return $this;
    }

    public function getDescr(): ?string
    {
        return $this->descr;
    }

    public function setDescr(?string $descr): static
    {
        $this->descr = $descr;

        return $this;
    }

    public function getPrerequis(): ?string
    {
        return $this->prerequis;
    }

    public function setPrerequis(string $prerequis): static
    {
        $this->prerequis = $prerequis;

        return $this;
    }

    public function getPrerequisArray(): array
    {
        return $this->prerequisArray;
    }

    public function setPrerequisArray(?array $prerequisArray): static
    {
        $this->prerequisArray = $prerequisArray;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role): static
    {
        $this->role = $role;

        return $this;
    }

    public function getRoleArray(): array
    {
        return $this->roleArray;
    }

    public function setRoleArray(?array $roleArray): static
    {
        $this->roleArray = $roleArray;

        return $this;
    }

    public function isIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): static
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): static
    {
        $this->slug = $slug;

        return $this;
    }

    public function getCompany(): ?CompanyUser
    {
        return $this->company;
    }

    public function setCompany(?CompanyUser $company): static
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return Collection<int, Applicants>
     */
    public function getApplicants(): Collection
    {
        return $this->applicants;
    }

    public function addApplicant(Applicants $applicant): static
    {
        if (!$this->applicants->contains($applicant)) {
            $this->applicants->add($applicant);
            $applicant->setOffer($this);
        }

        return $this;
    }

    public function removeApplicant(Applicants $applicant): static
    {
        if ($this->applicants->removeElement($applicant)) {
            // set the owning side to null (unless already changed)
            if ($applicant->getOffer() === $this) {
                $applicant->setOffer(null);
            }
        }

        return $this;
    }
}
