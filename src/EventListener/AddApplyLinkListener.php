<?php

namespace App\EventListener;

use App\Entity\Offer;

class AddApplyLinkListener
{
    private string $baseUrl;

    public function __construct(string $baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    public function postPersist($args): void
    {
        $entity = $args->getObject();

        if (!$entity instanceof Offer) {
            return;
        }

        $entityManager = $args->getObjectManager();

        $offerSlug = $entity->getSlug();
        $entity->setApplyUrl($this->baseUrl.'/offers/'.$offerSlug.'/apply');
        $entityManager->persist($entity);
        $entityManager->flush();
    }
}
