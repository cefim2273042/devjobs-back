<?php

namespace App\Controller;

use App\Entity\Applicants;
use App\Entity\Offer;
use App\Form\Applicant\ApplicantType;
use App\Repository\ApplicantsRepository;
use App\Repository\OfferRepository;
use App\Service\FileUploadService;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

#[Route('/offers', name: 'offers_')]
class OfferController extends AbstractController
{
    #[Route('/', name: 'index', methods: ['GET'])]
    public function index(OfferRepository $offerRepository,
        PaginatorInterface $paginator,
        Request $request): Response
    {
        $offers = $paginator->paginate(
            $offerRepository->findBy(['isActive' => 1], ['createdAt' => 'DESC']),
            $request->query->getInt('page', 1),
            15
        );

        return $this->render('Front/offer/index.html.twig', [
            'offers' => $offers,
        ]);
    }

    #[Route('/{slug}', name: 'show', methods: ['GET'])]
    public function show(Offer $offer): Response
    {
        return $this->render('Front/offer/show.html.twig', [
            'offer' => $offer,
        ]);
    }

    #[Route('/{slug}/apply', name: 'apply', methods: ['GET', 'POST'])]
    public function apply(Request $request,
        ApplicantsRepository $applicantsRepository,
        SluggerInterface $slugger,
        Offer $offer,
        FileUploadService $fileUploadService): Response
    {
        $applicant = new Applicants();
        $form = $this->createForm(ApplicantType::class, $applicant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $applicant->setOffer($offer);
            $applicant->setSlug($slugger->slug($applicant->getFirstName())->lower().'-'.uniqid());

            $resum = $form->get('resum')->getData();
            if ($resum) {
                $resumFileName = $fileUploadService->upload($resum);
                $applicant->setResum($resumFileName);
            }

            $applicantsRepository->save($applicant, true);

            $this->addFlash('success', 'Your application has been registered');

            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('applicant/create.html.twig', [
            'form' => $form,
            'offer' => $offer,
        ]);
    }
}
