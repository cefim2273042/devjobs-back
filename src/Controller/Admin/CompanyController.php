<?php

namespace App\Controller\Admin;

use App\Entity\CompanyUser;
use App\Form\Company\CompanyType;
use App\Repository\CompanyUserRepository;
use App\Service\FileUploadService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\String\Slugger\SluggerInterface;

#[Route('/admin/society', name: 'admin_company_')]
#[IsGranted('ROLE_ADMIN')]
class CompanyController extends AbstractController
{
    #[Route('/new', name: 'create', methods: ['GET', 'POST'])]
    public function create(Request $request,
        CompanyUserRepository $companyUserRepository,
        FileUploadService $fileUpload,
        SluggerInterface $slugger): Response
    {
        $user = new CompanyUser();
        $form = $this->createForm(\App\Form\RegisterType\CompanyType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $logo = $form->get('logo')->getData();

            if ($logo) {
                $logoFileName = $fileUpload->upload($logo);
                $user->setLogo($logoFileName);
            }
            $user->setRoles(['ROLE_COMPANY']);
            $user->setSlug($slugger->slug($user->getName())->lower().'-'.uniqid());
            $companyUserRepository->save($user, true);

            $this->addFlash('success', 'Society has been created');

            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('admin/company/create.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/{slug}/update', name: 'update', methods: ['GET', 'POST'])]
    public function update(Request $request,
        CompanyUserRepository $companyUserRepository,
        FileUploadService $fileService,
        CompanyUser $companyUser): Response
    {
        $form = $this->createForm(CompanyType::class, $companyUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $logo = $form->get('logo')->getData();

            if ($logo) {
                $logoFileName = $fileService->upload($logo);
                $companyUser->setLogo($logoFileName);
            }

            $companyUserRepository->save($companyUser, true);

            $this->addFlash('success', 'Society has been updated');

            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('admin/company/update.html.twig', [
            'form' => $form,
            'company' => $companyUser,
        ]);
    }

    #[Route('/{name}/delete', name: 'delete', methods: ['POST'])]
    public function delete(Request $request, CompanyUser $companyUser, CompanyUserRepository $companyUserRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$companyUser->getName(), $request->request->get('_token'))) {
            $companyUserRepository->remove($companyUser, true);
        }

        $this->addFlash('success', 'Society has been deleted');

        return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/', name: 'index', methods: ['GET'])]
    public function index(CompanyUserRepository $companyUserRepository): Response
    {
        return $this->render('admin/company/index.html.twig', [
            'companies' => $companyUserRepository->findAll(),
        ]);
    }

    #[Route('/{slug}', name: 'show', methods: ['GET'])]
    public function show(CompanyUser $companyUser): Response
    {
        return $this->render('admin/company/show.html.twig', [
            'company' => $companyUser,
        ]);
    }
}
