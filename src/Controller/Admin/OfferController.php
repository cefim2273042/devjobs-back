<?php

namespace App\Controller\Admin;

use App\Entity\Offer;
use App\Form\OfferType;
use App\Repository\ApplicantsRepository;
use App\Repository\OfferRepository;
use App\Service\SendMailService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\String\Slugger\SluggerInterface;

#[Route('/admin/offers', name: 'admin_offer_')]
#[IsGranted('ROLE_ADMIN')]
class OfferController extends AbstractController
{
    #[Route('/', name: 'index', methods: ['GET'])]
    public function index(OfferRepository $offerRepository): Response
    {
        return $this->render('offer/index.html.twig', [
            'offers' => $offerRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'create', methods: ['GET', 'POST'])]
    #[Route('/{slug}/update', name: 'edit', methods: ['GET', 'POST'])]
    public function create(Request $request,
        OfferRepository $offerRepository,
        SluggerInterface $slugger,
        ?string $slug): Response
    {
        $offerEntity = new Offer();
        if (null !== $slug) {
            $offerEntity = $offerRepository->findOneBy(['slug' => $slug]);
        }
        $user = $this->getUser();
        if (!$user) {
            $this->addFlash('danger', 'You have to be loggin');

            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
        }

        $form = $this->createForm(OfferType::class, $offerEntity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $offerEntity->setSlug($slugger->slug($offerEntity->getPostName())->lower().'-'.uniqid());
            $offerRepository->save($offerEntity, true);

            $this->addFlash('success', 'Offer created successfully');

            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('offer/new.html.twig', [
            'offer' => $offerEntity,
            'form' => $form,
        ]);
    }

    #[Route('/{slug}', name: 'show', methods: ['GET'])]
    public function show(Offer $offer): Response
    {
        return $this->render('offer/show.html.twig', [
            'offer' => $offer,
        ]);
    }

    #[Route('/{slug}/delete', name: 'delete', methods: ['POST'])]
    public function delete(Request $request,
        Offer $offer,
        OfferRepository $offerRepository,
        SendMailService $mailService): Response
    {
        $user = $this->getUser();

        if (!$user) {
            $this->addFlash('danger', 'You have to be loggin');

            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
        }

        if ($this->isCsrfTokenValid('delete'.$offer->getSlug(), $request->request->get('_token'))) {
            $applicants = $offer->getApplicants()->getValues();

            foreach ($applicants as $applicant) {
                $mailService->sendMail(
                    'no-reply@devjobs.com',
                    $applicant->getEmail(),
                    'The recruitment campaign for '.$offer->getPostName().' is now over.',
                    'endCampaign',
                    compact('offer', 'applicant')
                );
            }

            $offerRepository->remove($offer, true);
            $this->addFlash('success', 'Offer deleted successfully');
        }

        return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{slug}/applicants', name: 'applicants', methods: ['GET'])]
    public function showApplicantsByOffer(Offer $offer,
        ApplicantsRepository $applicantsRepository): Response
    {
        return $this->render('offer/applicant.html.twig', [
            'applicants' => $applicantsRepository->findBy(['offer' => $offer]),
        ]);
    }
}
