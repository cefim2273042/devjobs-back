<?php

namespace App\Controller\Admin;

use App\Entity\Applicants;
use App\Form\Applicant\ApplicantEditType;
use App\Form\Applicant\ApplicantType;
use App\Repository\ApplicantsRepository;
use App\Service\FileUploadService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\String\Slugger\SluggerInterface;

#[Route('/admin/applicants', name: 'admin_applicants_')]
#[IsGranted('ROLE_ADMIN')]
class ApplicantsController extends AbstractController
{
    #[Route('/new', name: 'create', methods: ['GET', 'POST'])]
    public function create(ApplicantsRepository $applicantsRepository,
        Request $request,
        FileUploadService $fileUploadService,
        SluggerInterface $slugger): Response
    {
        $applicant = new Applicants();
        $form = $this->createForm(ApplicantType::class, $applicant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $resum = $form->get('resum')->getData();

            if ($resum) {
                $resumFileName = $fileUploadService->upload($resum);
                $applicant->setResum($resumFileName);
            }
            $applicant->setSlug($slugger->slug($applicant->getFirstName())->lower().'-'.uniqid());
            $applicantsRepository->save($applicant, true);

            $this->addFlash('success', 'Applicant has been created');

            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('applicant/create.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/{slug}/update', name: 'update', methods: ['GET', 'POST'])]
    public function update(Applicants $applicants,
        Request $request,
        ApplicantsRepository $applicantsRepository): Response
    {
        $form = $this->createForm(ApplicantEditType::class, $applicants);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $applicantsRepository->save($applicants, true);

            $this->addFlash('success', 'Applicant has been updated');

            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('applicant/update.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/{slug}/delete', name: 'delete', methods: ['POST'])]
    public function delete(Request $request,
        Applicants $applicants,
        ApplicantsRepository $applicantsRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$applicants->getSlug(), $request->request->get('_token'))) {
            $resum = $applicants->getResum();
            if (null !== $resum) {
                $resumFilePath = $this->getParameter('kernel.project_dir').'/public/uploads/media/'.$resum;
                if (file_exists($resumFilePath)) {
                    unlink($resumFilePath);
                }
            }
            $applicantsRepository->remove($applicants, true);
        }

        $this->addFlash('success', 'Applicant has been deleted');

        return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/', name: 'index', methods: ['GET'])]
    public function index(ApplicantsRepository $applicantsRepository): Response
    {
        return $this->render('applicant/index.html.twig', [
            'applicants' => $applicantsRepository->findBy([], ['id' => 'DESC']),
        ]);
    }

    #[Route('/{slug}', name: 'show', methods: ['GET'])]
    public function show(Applicants $applicants): Response
    {
        return $this->render('applicant/show.html.twig', [
            'applicant' => $applicants,
        ]);
    }
}
