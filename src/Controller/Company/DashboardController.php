<?php

namespace App\Controller\Company;

use App\Repository\ApplicantsRepository;
use App\Repository\CompanyUserRepository;
use App\Repository\OfferRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

#[isGranted('ROLE_COMPANY')]
class DashboardController extends AbstractController
{
    #[Route('/my-dashboard', name: 'company_dashboard')]
    public function dashboard(OfferRepository $offerRepository,
        ChartBuilderInterface $chartBuilder,
        CompanyUserRepository $companyUserRepository,
        ApplicantsRepository $applicantsRepository): Response
    {
        $user = $this->getUser();
        $company = $companyUserRepository->find($user);

        $offers = $offerRepository->findBy(['company' => $company], ['createdAt' => 'ASC']);

        $countByDate = [];

        foreach ($offers as $offer) {
            $date = $offer->getCreatedAt()->format('d/m/Y');
            if (!isset($countByDate[$date])) {
                $countByDate[$date] = 0;
            }
            ++$countByDate[$date];
        }

        $labels = array_keys($countByDate);
        $data = array_values($countByDate);

        $chart = $chartBuilder->createChart(Chart::TYPE_LINE);

        $chart->setData([
            'labels' => $labels,
            'datasets' => [
                [
                    'label' => 'Nb of offers',
                    'backgroundColor' => 'rgb(34, 96, 112)',
                    'borderColor' => 'rgb(34, 96, 112)',
                    'data' => $data,
                ],
            ],
        ]);

        $chart->setOptions([]);

        return $this->render('company/dashboard.html.twig', [
            'chart' => $chart,
            'nbOffersOnLine' => $offerRepository->count(['company' => $company, 'isActive' => '1']),
            'applicantsUnread' => $applicantsRepository->findby(['offer' => $offers, 'isRead' => '0']),
            'nbApplicants' => $applicantsRepository->count(['offer' => $offers]),
        ]);
    }
}
