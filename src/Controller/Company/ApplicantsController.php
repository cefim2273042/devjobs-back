<?php

namespace App\Controller\Company;

use App\Entity\Applicants;
use App\Repository\ApplicantsRepository;
use App\Repository\CompanyUserRepository;
use App\Repository\OfferRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;

#[Route('my-applicants', name: 'company_applicants_')]
#[IsGranted('ROLE_COMPANY')]
class ApplicantsController extends AbstractController
{
    #[Route('/', name: 'index', methods: ['GET'])]
    public function index(ApplicantsRepository $applicantsRepository,
        CompanyUserRepository $companyUserRepository,
        OfferRepository $offerRepository): Response
    {
        $user = $this->getUser();
        $company = $companyUserRepository->find($user);

        if (!$company->getIsVerified()) {
            $this->addFlash('warning', 'You need to verify your account first');

            return $this->redirectToRoute('app_hoe');
        }

        $offers = $offerRepository->findBy(['company' => $company]);

        return $this->render('applicant/index.html.twig', [
            'applicants' => $applicantsRepository->findBy(['offer' => $offers]),
        ]);
    }

    #[Route('/{slug}', name: 'show', methods: ['GET'])]
    public function show(Applicants $applicants, ApplicantsRepository $applicantsRepository): Response
    {
        $applicants->setIsRead(true);
        $applicantsRepository->save($applicants, true);

        return $this->render('applicant/show.html.twig', [
            'applicant' => $applicants,
        ]);
    }

    #[Route('/{slug}/delete', name: 'delete', methods: ['POST'])]
    public function delete(Request $request,
        Applicants $applicants,
        ApplicantsRepository $applicantsRepository): Response
    {
        $user = $this->getUser();
        $company = $applicants->getOffer()->getCompany();

        if (!$user == $company) {
            $this->addFlash('warning', 'You are not allowed to delete this applicant');

            return $this->redirectToRoute('app_home');
        }

        if ($this->isCsrfTokenValid('delete'.$applicants->getSlug(), $request->request->get('_token'))) {
            $resum = $applicants->getResum();
            if (null !== $resum) {
                $resumFilePath = $this->getParameter('kernel.project_dir').'/public/uploads/media/'.$resum;
                if (file_exists($resumFilePath)) {
                    unlink($resumFilePath);
                }
            }
            if (password_verify($request->request->get('password'), $company->getPassword())) {
                $applicantsRepository->remove($applicants, true);

                $this->addFlash('success', 'Applicant has been deleted');
            } else {
                $this->addFlash('danger', 'Password is not correct');
            }

            return $this->redirectToRoute('company_applicants_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->redirectToRoute('company_applicants_index', [], Response::HTTP_SEE_OTHER);
    }
}
