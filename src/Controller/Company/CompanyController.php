<?php

namespace App\Controller\Company;

use App\Entity\CompanyUser;
use App\Form\Company\CompanyDeleteType;
use App\Form\RegisterType\CompanyType;
use App\Repository\CompanyUserRepository;
use App\Repository\UserRepository;
use App\Service\FileUploadService;
use App\Service\JWTService;
use App\Service\SendMailService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\String\Slugger\SluggerInterface;

#[Route(name: 'company_')]
class CompanyController extends AbstractController
{
    #[Route('/register', name: 'register', methods: ['GET', 'POST'])]
    public function register(Request $request,
        CompanyUserRepository $companyUserRepository,
        FileUploadService $fileUpload,
        SendMailService $sendMail,
        JWTService $jwt,
        SluggerInterface $slugger): Response
    {
        $user = new CompanyUser();
        $form = $this->createForm(CompanyType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $logo = $form->get('logo')->getData();
            if ($logo) {
                $logoFileName = $fileUpload->upload($logo);
                $user->setLogo($logoFileName);
            }
            $user->setRoles(['ROLE_COMPANY']);
            $user->setSlug($slugger->slug($user->getName())->lower().'-'.uniqid());
            $companyUserRepository->save($user, true);

            $this->addFlash('success', 'Your account has been created');

            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('security/register.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/my-account/update', name: 'update', methods: ['GET', 'POST'])]
    public function edit(Request $request,
        CompanyUserRepository $companyUserRepository,
        FileUploadService $fileUpload
    ): Response {
        $user = $this->getUser();
        $company = $companyUserRepository->find($user);

        if (!$company) {
            $this->addFlash('danger', 'You are not connected');

            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
        }

        if (!$company->getIsVerified()) {
            $this->addFlash('danger', 'Your account is not activated');

            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
        }

        $form = $this->createForm(\App\Form\Company\CompanyType::class, $company);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $logo = $form->get('logo')->getData();
            if ($logo) {
                $logoFileName = $fileUpload->upload($logo);
                $company->setLogo($logoFileName);
            }
            $companyUserRepository->save($company, true);

            $this->addFlash('success', 'Your account has been updated');

            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('company/update.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/my-account/delete', name: 'delete', methods: ['GET', 'POST'])]
    public function delete(Request $request,
        CompanyUserRepository $companyUserRepository,
        SendMailService $sendMail,
        UserRepository $userRepository): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $user = $companyUserRepository->find($this->getUser());
        $admin = $userRepository->findOneBy(['username' => 'admin']);

        if (!$user) {
            $this->addFlash('danger', 'You are not connected');

            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
        }

        if (!$user->getIsVerified()) {
            $this->addFlash('danger', 'Your account is not activated');

            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
        }

        if ($user->getIsDelete()) {
            $this->addFlash('danger', 'Your account is already deleted');

            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
        }

        $form = $this->createForm(CompanyDeleteType::class);
        $form->setData([
            'name' => $user->getName(),
            'lastname' => $user->getLastname(),
            'firstname' => $user->getFirstname(),
            'email' => $user->getEmail(),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setIsDelete(true);
            $companyUserRepository->save($user, true);

            $motif = $form->get('motif')->getData();
            $context = [
                'user' => $user,
                'admin' => $admin,
                'motif' => $motif,
            ];

            $sendMail->sendMail(
                'no-reply@devjobs.com',
                $user->getEmail(),
                'Account deletion request',
                'delete_account_company',
                $context
            );

            $sendMail->sendMail(
                'no-reply@devjobs.com',
                $admin->getEmail(),
                'Account deletion request',
                'delete_account',
                $context
            );

            $this->addFlash('success', 'Your request has been sent to the administrator');

            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('company/delete.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route('/activate/{token}', name: 'activate')]
    public function verifyCompany(string $token, JWTService $jwt, UserRepository $userRepository): Response
    {
        // Check du token
        if ($jwt->isValid($token) && !$jwt->isExpired($token) && $jwt->check($token, $this->getParameter('app.jwtsecret'))) {
            $payload = $jwt->getPayload($token);
            $user = $userRepository->findOneBy(['username' => $payload['user_identifiant']]);

            if ($user && !$user->getIsVerified()) {
                $user->setIsVerified(true);
                $userRepository->save($user, true);

                $this->addFlash('success', 'Your account has been activated');

                return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
            }
        }
        $this->addFlash('danger', 'Token is invalid or expired');

        return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/resend', name: 'resend')]
    public function resendVerif(JWTService $jwt, SendMailService $sendMail, UserRepository $userRepository): Response
    {
        $user = $userRepository->find($this->getUser());

        if (!$user) {
            $this->addFlash('danger', 'You are not connected');

            return $this->redirectToRoute('app_login', [], Response::HTTP_SEE_OTHER);
        }

        if ($user->getIsVerified()) {
            $this->addFlash('danger', 'Your account is already activated');

            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
        }

        $header = [
            'typ' => 'JWT',
            'alg' => 'HS256',
        ];

        $payload = [
            'user_identifiant' => $user->getUserIdentifier(),
        ];

        $token = $jwt->generate($header, $payload, $this->getParameter('app.jwtsecret'));

        $sendMail->sendMail(
            'no-reply@devjobs.fr',
            $user->getEmail(),
            'Activate your account',
            'register',
            compact('user', 'token')
        );

        $this->addFlash('success', 'A new email has been sent');

        return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/my-account', name: 'profil')]
    #[IsGranted('ROLE_COMPANY')]
    public function profil(CompanyUserRepository $companyUserRepository): Response
    {
        $user = $this->getUser();
        $companyUser = $companyUserRepository->find($user);

        if (!$companyUser->getIsVerified()) {
            $this->addFlash('danger', 'Your account is not activated');

            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('company/profil.html.twig', [
            'user' => $user,
        ]);
    }
}
