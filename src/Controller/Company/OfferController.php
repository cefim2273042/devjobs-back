<?php

namespace App\Controller\Company;

use App\Entity\Offer;
use App\Form\OfferType;
use App\Repository\ApplicantsRepository;
use App\Repository\CompanyUserRepository;
use App\Repository\OfferRepository;
use App\Service\SendMailService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\String\Slugger\SluggerInterface;

#[Route('/my-offers', name: 'company_offers_')]
#[IsGranted('ROLE_COMPANY')]
class OfferController extends AbstractController
{
    #[Route('/', name: 'index', methods: ['GET'])]
    public function index(OfferRepository $offerRepository): Response
    {
        $user = $this->getUser();

        if (!$user) {
            $this->addFlash('danger', 'You have to be loggin as company');

            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('offer/index.html.twig', [
            'offers' => $offerRepository->findBy(['company' => $user]),
        ]);
    }

    #[Route('/new', name: 'new', methods: ['GET', 'POST'])]
    #[Route('/{slug}/update', name: 'edit', methods: ['GET', 'POST'])]
    public function create(OfferRepository $offerRepository, Request $request, SluggerInterface $slugger, CompanyUserRepository $companyUserRepository, ?string $slug): Response
    {
        $offerEntity = new Offer();

        if (null !== $slug) {
            $offerEntity = $offerRepository->findOneBy(['slug' => $slug]);
        }

        $user = $this->getUser();
        $company = $companyUserRepository->find($user);

        $form = $this->createForm(OfferType::class, $offerEntity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $offerEntity->setSlug($slugger->slug($offerEntity->getPostName())->lower().'-'.uniqid());
            $offerEntity->setCompany($company);
            $offerRepository->save($offerEntity, true);

            $this->addFlash('success', 'Offer created successfully');

            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('offer/new.html.twig', [
            'offer' => $offerEntity,
            'form' => $form,
        ]);
    }

    #[Route('/{slug}', name: 'show', methods: ['GET'])]
    public function show(Offer $offer): Response
    {
        $user = $this->getUser();

        if (!$user) {
            $this->addFlash('danger', 'You have to be loggin as company');

            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
        }

        if ($user !== $offer->getCompany()) {
            $this->addFlash('danger', 'You are not allowed to see this offer');

            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('offer/show.html.twig', [
            'offer' => $offer,
        ]);
    }

    #[Route('/{slug}/delete', name: 'delete', methods: ['POST', 'GET'])]
    public function delete(Request $request,
        Offer $offer,
        OfferRepository $offerRepository,
        SendMailService $mailService): Response
    {
        $user = $this->getUser();

        if (!$user) {
            $this->addFlash('danger', 'You have to be loggin as company');

            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
        }

        if ($user !== $offer->getCompany()) {
            $this->addFlash('danger', 'You are not allowed to delete this offer');

            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
        }

        if ($this->isCsrfTokenValid('delete'.$offer->getSlug(), $request->request->get('_token'))) {
            if (password_verify($request->request->get('password'), $user->getPassword())) {
                $applicants = $offer->getApplicants()->getValues();

                foreach ($applicants as $applicant) {
                    $mailService->sendMail(
                        'no-reply@devjobs.com',
                        $applicant->getEmail(),
                        'The recruitment campaign for '.$offer->getPostName().' is now over.',
                        'endCampaign',
                        compact('offer', 'applicant')
                    );
                }

                $offerRepository->remove($offer, true);
                $this->addFlash('success', 'Offer deleted successfully');
            } else {
                $this->addFlash('danger', 'Wrong password');

                return $this->redirectToRoute('company_offers_index', [], Response::HTTP_SEE_OTHER);
            }
        }

        return $this->redirectToRoute('company_offers_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{slug}/applicant', name: 'applicants', methods: ['GET'])]
    public function getApplicant(ApplicantsRepository $applicantsRepository,
        Offer $offer): Response
    {
        $user = $this->getUser();

        if (!$user) {
            $this->addFlash('danger', 'You have to be loggin as company');

            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
        }

        if ($user !== $offer->getCompany()) {
            $this->addFlash('danger', 'You are not allowed to delete this offer');

            return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('offer/applicant.html.twig', [
            'applicants' => $applicantsRepository->findBy(['offer' => $offer]),
        ]);
    }
}
