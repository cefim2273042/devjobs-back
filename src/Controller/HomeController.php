<?php

namespace App\Controller;

use App\Repository\OfferRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home', methods: ['GET'])]
    public function index(OfferRepository $offerRepository): Response
    {
        return $this->render('index.html.twig', [
            'offers' => $offerRepository->findBy(['isActive' => 1], ['id' => 'DESC'], 10),
        ]);
    }

    #[Route('/reset-password', name: 'home_reset_password', methods: ['GET'])]
    public function resetPassword(): Response
    {
        return $this->render('security/reset_password.html.twig', [
        ]);
    }
}
