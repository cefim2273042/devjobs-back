<?php

namespace App\DataFixtures;

use App\Entity\Applicants;
use App\Entity\CompanyUser;
use App\Entity\Offer;
use App\Entity\WebsiteUser;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

class AppFixtures extends Fixture
{
    private UserPasswordHasherInterface $passwordHasher;
    private SluggerInterface $slugger;

    public function __construct(UserPasswordHasherInterface $passwordHasher,
        SluggerInterface $slugger)
    {
        $this->passwordHasher = $passwordHasher;
        $this->slugger = $slugger;
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('en_EN');

        // ///////////////////// make admin ///////////////////////////////////
        $admin = new WebsiteUser();
        $admin->setUsername('Admin')
            ->setEmail('admin@admin.fr')
            ->setLastname('Admin')
            ->setFirstname('Admin')
            ->setRoles(['ROLE_ADMIN'])
            ->setIsVerified(true)
            ->setPassword($this->passwordHasher->hashPassword($admin, 'Admin1.'));

        $manager->persist($admin);

        // ///////////////////// Make some society ///////////////////////////////////
        for ($c = 0; $c < mt_rand(5, 10); ++$c) {
            $company = new CompanyUser();
            $company->setUsername($faker->userName)
                ->setEmail($faker->email)
                ->setLastname($faker->lastName)
                ->setFirstname($faker->firstName())
                ->setRoles(['ROLE_COMPANY'])
                ->setPassword($this->passwordHasher->hashPassword($company, 'Company1.'))
                ->setIsVerified($faker->boolean())
                ->setLogo('logo-'.$faker->numberBetween(1, 3).'.svg')
                ->setColor($faker->hexColor)
                ->setLocation($faker->countryCode())
                ->setWebsite($faker->url())
                ->setName($faker->company())
                ->setIsDelete($faker->boolean())
                ->setSlug(strtolower($this->slugger->slug($company->getName()).'-'.mt_rand(1, 100)));

            $manager->persist($company);

            // ///////////////////// Make some offers ///////////////////////////////////
            for ($o = 0; $o < mt_rand(1, 5); ++$o) {
                $offer = new Offer();
                $offer->setPostName($faker->jobTitle())
                    ->setCompany($company)
                    ->setContractType(mt_rand(1, 4))
                    ->setDescr($faker->text(150))
                    ->setPrerequis($faker->text(50))
                    ->setRole($faker->text(50))
                    ->setIsActive($faker->boolean())
                    ->setCreatedAt(\DateTimeImmutable::createFromMutable($faker->dateTimeBetween('-6 months', 'now')))
                    ->setSlug(strtolower($this->slugger->slug($offer->getPostName()).'-'.mt_rand(1, 100)));

                $manager->persist($offer);

                // ///////////////////// Make some applies ///////////////////////////////////
                for ($a = 0; $a < mt_rand(1, 10); ++$a) {
                    $applicant = new Applicants();
                    $applicant->setOffer($offer)
                        ->setFirstname($faker->firstName())
                        ->setLastname($faker->lastName())
                        ->setEmail($faker->email())
                        ->setPhoneNumber('06'.mt_rand(10000000, 99999999))
                        ->setResum('cv-64a910b817e8a.pdf')
                        ->setComment($faker->text(50))
                        ->setIsRead($faker->boolean())
                        ->setSlug(strtolower($this->slugger->slug($applicant->getFirstname()).'-'.mt_rand(1, 100)));

                    $manager->persist($applicant);
                }
            }
        }

        $manager->flush();
    }
}
