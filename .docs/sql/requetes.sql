USE devjobs;

    -- 1. Récupération des offres par localisation
    SELECT * FROM offer, company_user WHERE company_user.location = 'EN' AND company_user.id = offer.company_id;


    -- 2. Récupération des offres par type de contrat
    SELECT * FROM offer WHERE contract_type = '2';

    -- 3. Récupération de toutes les offres, classés par type de contrat
    SELECT * FROM offer ORDER BY contract_type;