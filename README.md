# DevJobs Back

## Description
Après le développement du front de DevJobs, on passe au back avec l'aide de Symfony et Api-Platform !

## Installation
* Clone du repertoire sur votre machine local
* Créer un fichier .env.local basé sur le .env et complétez les variables ainsi que la base de données que vous osuhaitez créer

Installer le projet :
`composer install`  
Installer les dépendances npm : 
`npm install`  
Faire un build pour webpack :
`npm run build`  
Créer la base de données : 
`symfony console d:d:c`
Lancer la migration de la base de données :
`symfony console d:m:m`  
Lancer les fixtures pour peupler la base :
`symfony console d:f:l`  
Lancer le serveur :
`symfony serve`  

## Utiliser le site :
Vous pouvez trouver les identifiants de connexion pour l'admin et pour les compagnies dans le fichier des fixtures /src/DataFixtures/AppFixtures.php  
DevJobs-Back faisant de l'envoi de mail, il est conseillé de faire tourner MailHog (ou autre) sur le port 1025 afin d'éviter les erreurs lors des envois de mail. Il est également possible de voir les mails dans le profiler de symfony.  

Le projet est accompagné d'un fichier makeFile que vous pouvez utiliser pour lancer les commandes symfony / composer / docker / npm.
